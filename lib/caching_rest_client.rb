require 'rest_client'

module CachingRestClient
  class Factory
    def initialize(cache)
      @cache = cache
    end

    def new(*args, &block)
      Resource.new(*args, &block).tap do |r|
        r.options[:_cache] = @cache
      end
    end
  end

  class Resource < ::RestClient::Resource
    def get(additional_headers={}, &block)
      cache.fetch(url, :get, additional_headers) do
        super(additional_headers, &block)
      end
    end

    def head(additional_headers={}, &block)
      cache.fetch(url, :head, additional_headers) do
        super(additional_headers, &block)
      end
    end

    private

    def cache
      options.fetch(:_cache)
    end
  end
end
