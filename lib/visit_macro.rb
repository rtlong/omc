require 'core_ext/accept'

module VisitMacro
  def self.included(base)
    base.extend(ClassMethods)
  end

  def visit(visitable)
    meth = visit_methods(visitable.class).detect { |m| respond_to?(m) }

    return send(meth, visitable) if meth

    unless self.class.ignore_undefined_visits
      raise format("%s doesn't know how to visit %s", self.class, visitable.class)
    end
  end

  private

  def visit_methods(visitable_class)
    visitable_class.ancestors.map do |klass|
      :"visit_#{klass.name}"
    end
  end

  module ClassMethods
    def visits(klass, &block)
      unless block.arity == 1
        fail ArgumentError.new("block must accept visited object")
      end
      define_method(:"visit_#{klass.name}", &block)
    end

    attr_accessor :ignore_undefined_visits
  end
end
