require 'delegate'

# Run Ruby with warnings enabled. Without going crazy.
# courtesy https://gist.github.com/rkh/9130314
module Support
  class WarningFilter < DelegateClass(IO)
    def write(line)
      super if line !~ %r[^\S+ruby/gems/\S+:\d+: warning:]
    end
  end
end

$stderr = Support::WarningFilter.new($stderr)
