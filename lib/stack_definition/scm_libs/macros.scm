(define-syntax resource
  (syntax-rules ()
                ((resource type _name deps props ...)
                 (let ((resource-name _name))
                   (register-resource _name deps (type props ...))))))

(define-syntax action
  (syntax-rules ()
                ((action type _name deps props ...)
                 (let ((resource-name _name))
                   (register-resource _name deps (type props ...))))))

(define-syntax map-prop-over-ids
  (syntax-rules ()
                ((map-prop-over-ids prop ids)
                 (map (lambda (id)
                        `(,id prop))
                      ids))))
