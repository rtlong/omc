module StackDefinition
  module Attributes
    class Attribute
      def initialize(name, type: nil)
        @name = name
        @valid_types = Array(type)
      end

      attr_reader :valid_types, :name

      def validate(resource, value)
        return if value.nil?

        if valid_types.any? and valid_types.none? { |type| value.is_a?(type) }
          raise ArgumentError.new("#{resource.resource_full_name}: invalid value for `#{name}`: #{value.inspect}")
        end

        if value.respond_to?(:validate!)
          value.validate!
        end
      end
    end

    class Collection < Attribute
      def validate(resource, value)
        return if value.nil?

        value.each do |e|
          super(resource, e)
        end
      end
    end
  end
end
