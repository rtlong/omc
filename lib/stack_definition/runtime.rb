require 'heist'
require 'configured_instance'
require 'stack_definition/resource_definition'
require 'stack_definition/resource_query'
require 'stack_definition/resources_collection'
require 'stack_definition/resource_declaration'
require 'stack_definition/dependency'

module StackDefinition
  class Runtime
    include ConfiguredInstance

    def initialize(config, resource_types_map, resources_collection=ResourcesCollection.new)
      super config
      @resource_types_map = resource_types_map
      @resources_collection = resources_collection
    end

    attr_reader :resources_collection

    def parse(filename)
      heist_runtime.run filename
      self
    rescue => ex
      if /private method `eval'/.match(ex.message)
        config.ui.fatal("Looks like you may have a syntax error in the stack definition")
      end
      raise
    end

    def with_new_scope
      scope = Heist::Runtime::Scope.new(heist_runtime.user_scope)
      return scope unless block_given?
      yield scope
    end

    private

    def heist_runtime
      @heist_runtime ||= setup_runtime(Heist::Runtime.new)
    end

    def setup_runtime(runtime)
      # Set a stack-definition-level configuration variable
      runtime.define 'configure' do |configuration_variable, value|
        config.stack_config[HeistHelpers.identifier_to_sym(configuration_variable)] = value
        runtime.user_scope[configuration_variable] = value
        value
      end

      runtime.define 'register-resource' do |resource_name, deps, resource|
        r = ResourceDeclaration.new(resource, resource_name, deps)
        @resources_collection.register(r)
        config.ui.debugf "Resource Registered: %s => %s", r.resource_full_name, r.dependencies.map(&:resource_full_name)
      end

      @resource_types_map.each_pair do |name, klass|
        runtime.define name do |*props|
          begin
          klass.new(*props)
          rescue
            config.ui.errorf "Error while instantiating new %s (%s) resource with props %s", name, klass, props.inspect
            raise
          end
        end
      end

      runtime.define 'dep' do |local_name, type_id, resource_name|
        resource_class = @resource_types_map.fetch(HeistHelpers.identifier_to_sym(type_id))
        Dependency.new(local_name, resource_class, resource_name)
      end

      runtime.define 'format' do |fstr, *args|
        format(fstr, *args)
      end

      runtime.define 'join' do |joinstr, *args|
        args.join(joinstr)
      end

      runtime.define 'string-gsub' do |pattern, replacement, string|
        string.gsub(Regexp.new(pattern), replacement)
      end

      runtime.define 'string-ends-with?' do |suffix, string|
        string.end_with?(suffix)
      end

      runtime.define 'fetch' do |key, ruby_hash|
        ruby_hash[key]
      end

      runtime.define 'pp' do |label, value|
        p({ label => value })
      end

      runtime.define 'raise' do |message|
        fail message
      end

      Dir[File.expand_path('../scm_libs/*.scm', __FILE__)].each do |scm_file|
        config.ui.announce("Loading SCM lib: #{scm_file}") do
          runtime.run scm_file
        end
      end

      runtime
    end
  end
end
