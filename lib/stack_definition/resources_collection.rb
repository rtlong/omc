require 'stack_definition/errors'

module StackDefinition
  class ResourcesCollection
    def initialize
      @collection = {}
    end

    def register(resource_declaration)
      if @collection.key?(resource_declaration.resource_full_name)
        raise AlreadyDefinedError.new(resource_declaration)
      end
      @collection[resource_declaration.resource_full_name] = resource_declaration
      resource_declaration.resource_collection = self
    end

    def find_by_name(resource_full_name)
      @collection.fetch(resource_full_name)
    end

    def all
      @collection.values
    end
    alias_method :to_a, :all

    def validate_dependency_references!
      all.each do |r|
        r.validate_dependency_references!
      end
    end

    def validate_deferred_evaluations!(config, scope)
      all.each do |r|
        begin
          r.validate_deferred_evaluations!(scope)
        rescue
          config.ui.error "Error while calculating deferred references for #{r.resource_full_name}"
          raise
        end
      end
    end

    def accept(visitor)
      all.each do |r|
        visitor.visit r
      end
    end

    def nonprovisioned
      all.reject(&:provisioned?)
    end

    # for each *non-provisioned* resource, yield to block in order of dependency
    # chain
    def each_nonprovisioned(&block)
      ready, not_ready = [], nonprovisioned.shuffle

      while not_ready.any?
        not_ready_names = not_ready.map(&:resource_full_name)
        ready, not_ready = not_ready.partition { |r|
          (r.dependencies.map(&:resource_full_name) & not_ready_names).empty?
        }
        if ready.empty?
          binding.pry
          raise "circular dependency?"
        end
        ready.each { |r| block.call(r) }
      end
    end
  end
end
