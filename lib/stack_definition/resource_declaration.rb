require 'stack_definition/errors'
require 'stack_definition/resource_naming'

module StackDefinition
  class ResourceDeclaration
    include ResourceNaming

    def initialize(resource, resource_name, deps)
      @resource = resource
      @resource.resource_name = resource_name
      self.resource_name = resource_name
      self.dependencies = deps
    end

    attr_accessor :resource
    attr_accessor :resource_collection
    attr_reader :dependencies

    def dependencies=(dependencies)
      dependencies = dependencies.to_ruby.flatten
      unless dependencies.all? { |d| d.is_a?(Dependency) }
        raise ArgumentError, "#dependencies must be a collection of Dependency objects. Got #{dependencies.inspect}"
      end

      local_names = dependencies.map(&:local_name).compact
      unless local_names == local_names.uniq
        raise ArgumentError, "#dependencies local_names must each be unique or falsy. Got #{local_names}"
      end
      @dependencies = dependencies
    end

    def reevaluate_with_scope(scope)
      self.resource = resource.reevaluate_with_scope(scope)
      self
    end

    def resource_class
      resource.class
    end

    def provisioned?
      resource.provisioned?
    end

    def validate_dependency_references!
      dependencies.each do |dep|
        begin
          dep.lookup_resource(resource_collection)
        rescue KeyError
          raise UnknownDependencyReference.new(self, dep)
        end
      end
    end

    def validate_deferred_evaluations!(heist_scope)
      scope = define_dependencies_on_scope(heist_scope.dup)
      resource.reevaluate_with_scope(scope).validate!
    end

    def launch(scope, config)
      ensure_all_dependcies_provisioned!
      define_dependencies_on_scope(scope)
      reevaluate_with_scope(scope)
      resource.launch(config)
    end

    def ensure_all_dependcies_provisioned!
      dependencies.each do |dep|
        dep_resource = dep.lookup_resource(resource_collection)
        dep_resource.provisioned? or fail ExecutionOutOfOrder.new(self, dep_resource)
      end
    end


    private

    # defines variables for each referenced dependency using the local_name
    # given, with the current value of that dependency
    def define_dependencies_on_scope(scope)
      dependencies.each do |dep|
        next unless dep.local_name
        scope.define(dep.local_name) { |property|
          r = dep.lookup_resource(resource_collection)
          r.resource[property]
        }
      end

      scope
    end
  end
end
