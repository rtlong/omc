module StackDefinition
  module Validations
    class ValidationError < StandardError
      def initialize(value, attribute, object, message=nil)
        @attribute, @value, @object, @message = attribute, value, object, message
      end

      def to_s
        loc = [*@object.key_path, @attribute].join('.')
        [
          "Value `#{@value.inspect}` for #{@object.class.name}##{@attribute} invalid @ #{loc}",
          @message,
        ].compact.join(': ')
      end
    end

    class ValidatorChain
      def initialize
        @validators = []
      end

      attr_reader :validators

      def present(*args)
        append(PresenceValidator.new(*args))
      end

      def type(*args)
        append(TypeValidator.new(*args))
      end

      def nullable
        append -> (value, _) { value or throw(:stop_validation, nil) }
      end

      def lambda(message=nil, &callable)
        append(LambdaValidator.new(callable, message))
      end

      def non_empty(*args)
        append(NonEmptyValidator.new(*args))
      end

      def greater_than(threshold)
        append(NumericComparisonValidator.new(:>, threshold))
      end

      # returns the first validator which failed
      def validate(value, resource)
        catch(:stop_validation) do
          validators.each do |validator|
            validator.call(value, resource) or return validator
          end
        end
        nil
      end

      private

      def append(validator)
        new_chain = dup
        new_chain.validators << validator
        new_chain
      end
    end

    class LambdaValidator
      def initialize(callable, message=nil)
        @callable = callable
        @message = message
      end

      def call(value, resource)
        @callable.call(value, resource)
      end

      def message
        @message || "must satisfy lambda validator @ #{@callable.source_location.join(':')}"
      end
    end

    class NumericComparisonValidator
      def initialize(comparator, operand)
        @comparator = comparator
        @operand = operand
      end

      def call(value, _)
        value.to_i.send(@comparator, @operand)
      end

      def message
        ['must be', @comparator.to_s, @operand.to_s].join(' ')
      end
    end

    class PresenceValidator
      def call(value, _)
        !value.nil?
      end

      def message
        'must be set'
      end
    end

    class NonEmptyValidator
      def call(value, _)
        value.respond_to?(:empty?) ? !value.empty? : true
      end

      def message
        'must not be empty'
      end
    end

    class TypeValidator
      def initialize(klass)
        @klass = klass
      end

      def call(value, _)
        value.is_a?(@klass)
      end

      def message
        "must be an instance of #{@klass}"
      end
    end
  end
end
