require 'stack_definition/resource_ontology/dsl'
require 'stack_definition/resource_ontology/ontology_attributes'

module StackDefinition
  module ResourceOntology
    module Helpers
      def self.ontology_name_to_const_name(item_name)
        item_name.to_s.gsub(/(?:\A|_)(\w)/) { |m| $1.upcase }
      end
    end

    include DSL
    extend OntologyAttributes

    def definitions
      @definitions ||= {}
    end

    def ontology_path
      nil
    end

    def resource_ontology
      self
    end

    def register_resource(klass)
      definitions[klass.ontology_path.to_sym] = klass
    end

    def lookup(type)
      case type
      when Class
        type
      when String
        definitions.fetch(type.to_sym)
      end
    end
  end
end
