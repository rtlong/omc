require 'stack_definition/heist_helpers'

module StackDefinition
  module ResourceNaming
    attr_accessor :resource_class

    def resource_name=(name)
      @resource_name = HeistHelpers.name_from_heist(name)
    end

    attr_reader :resource_name

    def resource_full_name
      [resource_class.ontology_path, resource_name].join(':')
    end
  end
end
