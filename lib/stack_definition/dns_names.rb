module StackDefinition
  module DNSNames
    def fqdn
      [name._value, stack.fqdn].compact.join('.')
    end
  end
end
