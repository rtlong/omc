require 'set'
require 'heist'

require 'stack_definition/errors'
require 'stack_definition/resource_naming'
require 'stack_definition/heist_helpers'
require 'stack_definition/attributes'
require 'stack_definition/resource_ontology/ontology_attributes'

module StackDefinition
  class ResourceDefinition
    extend ResourceOntology::OntologyAttributes
    extend Forwardable

    class <<self
      attr_accessor :launch_proc

      def attributes
        @attributes ||= {}
      end

      def attribute(name, type: nil)
        attributes[name] = Attributes::Attribute.new(name, type: lookup_types(type))
        attr_accessor name
      end

      def collection(name, type: nil)
        attributes[name] = Attributes::Collection.new(name, type: lookup_types(type))
        define_method(name) do
          instance_variable_get(:"@#{name}") || []
        end
        define_method(:"#{name}=") do |value|
          instance_variable_set(:"@#{name}", value.is_a?(Array) ? value : [value])
        end
      end

      def lookup_types(types)
        Array(types).map { |t| resource_ontology.lookup(t) }
      end

      def inherited(subclass)
        subclass.attributes.update(attributes)
      end

      def launch_via(launcher_class=nil, &block)
        if block_given?
          self.launch_proc = block
        elsif launcher_class.respond_to?(:new)
          launch_method = :"launch_#{ontology_name}"
          # Sanity check:
          unless launcher_class.instance_methods.include?(launch_method)
            raise "Launcher class #{launcher_class} doesn't know how to launch #{ontology_name}"
          end
          self.launch_proc = -> (config, resource) do
            launcher_class.new(config).send(launch_method, resource)
          end
        else
          raise ArgumentError
        end
      end
    end

    include ResourceNaming

    def initialize(*props)
      unless props.length % 2 == 0
        fail ArgumentError, 'Odd number of props. ' \
          'Props need to be expressed as (key value) tuples.'
      end

      while props.any?
        self[props.shift] = props.shift
      end
    end

    attr_accessor :provisioned

    def provisioned?
      !!provisioned
    end

    def [](key)
      self.public_send(key.to_s.to_sym)
    end

    def []=(key, value)
      methname = :"#{key}="
      if !respond_to?(methname)
        raise UnknownAttributeError.new(key, self)
      end
      self.class.attributes.fetch(HeistHelpers.identifier_to_sym(key)) do
        raise SetterIsNotAttributeError.new(key, self)
      end
      self.public_send(methname, value)
    end

    def validate!
      self.class.attributes.each_pair do |name, attr|
        attr.validate(self, self[name])
      end
    end

    def reevaluate_with_scope(scope)
      raise 'already provisioned. what the hell r u doing.' if provisioned?

      dup.tap do |copy|
        copy.each_attribute do |attr, value|
          next if value.nil?
          copy[attr] = HeistHelpers.evaluate_item_with_scope(value, scope)
        end
      end
    end

    def accept(visitor)
      visitor.visit(self)
    end

    def launch(config)
      launcher = self.class.launch_proc or
        return config.ui.warnf('No launcher defined for %s', resource_name)

      launcher.call(config, self)
    end

    def each_attribute
      self.class.attributes.each_key do |attr|
        value = self[attr]
        yield(attr, value)
      end
    end

    def to_hash
      Hash.new.tap do |h|
        each_attribute do |attr, value|
          h[attr] = value.respond_to?(:to_hash) ? value.to_hash : value
        end
      end
    end

    def to_scm
      s = "(#{self.class.ontology_path}"
      each_attribute do |attr, value|
        next if value.nil?
        s << "\n"
        s << attr.to_scm
        s << ' '
        s << value.to_scm
      end
      s << ')'
    end

    protected

    def resource_class
      self.class
    end
  end
end
