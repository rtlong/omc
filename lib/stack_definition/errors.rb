module StackDefinition
  class AlreadyDefinedError < StandardError
    def initialize(resource_decl)
      @resource_decl = resource_decl
    end

    def to_s
      "Resource #{@resource_decl.resource_full_name} is defined more than once"
    end
  end

  class UnknownDependencyReference < StandardError
    def initialize(resource, dependency)
      @resource, @dependency = resource, dependency
    end

    def to_s
      "Resource #{@resource.resource_full_name} depends on undefined resource #{@dependency.resource_full_name}"
    end
  end

  class UnknownAttributeError < StandardError
    def initialize(key, resource)
      @key, @resource = key, resource
    end

    def to_s
      "There is no Attribute named `#{@key}` for #{@resource.resource_full_name}"
    end
  end

  class SetterIsNotAttributeError < UnknownAttributeError
    def to_s
      "`#{@key}` is a valid setter but is not an Attribute for #{@resource.resource_full_name}; it is invalid to set it in the stack definition."
    end
  end

  class ExecutionOutOfOrder < StandardError
    def initialize(resource, missing_dep)
      @resource = resource
      @missing_dep = missing_dep
    end

    def to_s
      "Not all dependencies are provisioned for #{@resource.resource_full_name}: #{@missing_dep.resource_full_name} is not provisioned"
    end
  end
end
