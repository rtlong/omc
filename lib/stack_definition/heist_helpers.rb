require 'heist'

module StackDefinition
  module HeistHelpers
    def self.name_from_heist(node)
      case node
      when String
        return node
      when NilClass
        return nil
      when Heist::Runtime::Identifier
        return node.to_s
      else
        return nil if node.is_a?(Heist::Runtime::Cons) and node.to_ruby.empty?
      end
      raise ArgumentError, "Unsure how to handle #{node} as name"
    end

    def self.evaluate_item_with_scope(item, scope)
      item.respond_to?(:reevaluate_with_scope) and return item.reevaluate_with_scope(scope)

      if item.is_a?(Heist::Runtime::Cons) && item.car.is_a?(Heist::Runtime::Identifier)
        return item.eval(scope)
      end

      case item
      when Enumerable
        return item.map { |e| evaluate_item_with_scope(e, scope) }
      when Heist::Runtime::Identifier
        return item.eval(scope)
      when String, Numeric, Symbol, true, false, nil
        return item
      else
        raise ArgumentError, "Unsure how to evaulate #{item.inspect}"
      end
    end

    def self.identifier_to_sym(identifier)
      case identifier
      when Heist::Runtime::Identifier
        return identifier.to_ruby
      when Heist::Runtime::Function
        return identifier.name.to_sym
      when Symbol, String
        return identifier.to_sym
      else
        raise "Unsure how to convert #{identifier.class}:#{identifier.inspect} to Symbol"
      end
    end
  end
end
