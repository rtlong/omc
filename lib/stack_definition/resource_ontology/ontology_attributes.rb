module StackDefinition
  module ResourceOntology
    module OntologyAttributes
      attr_accessor :ontology_name
      attr_accessor :ontology_parent

      def ontology_path
        [ontology_parent.ontology_path, ontology_name].compact.join('/')
      end

      def register_resource(klass)
        ontology_parent.register_resource(klass)
      end

      def resource_ontology
        ontology_parent.resource_ontology
      end
    end
  end
end
