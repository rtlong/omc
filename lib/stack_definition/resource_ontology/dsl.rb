require 'stack_definition/resource_definition'
require 'stack_definition/resource_ontology/ontology_attributes'
require 'stack_definition/resource_query'

module StackDefinition
  module ResourceOntology
    module DSL
      def namespace(name, &block)
        parent = self

        const_name = constant_name(name)

        if parent.const_defined?(const_name, false)
          mod = parent.const_get(const_name, false)
        else
          mod = Module.new
          parent.const_set(const_name, mod)

          mod.extend StackDefinition::ResourceOntology::OntologyAttributes
          mod.ontology_name = name
          mod.ontology_parent = parent

          mod.extend StackDefinition::ResourceOntology::DSL
        end

        mod.module_eval &block
      end

      def resource(name, superclass=ResourceDefinition, &block)
        parent = self

        const_name = constant_name(name)

        if parent.const_defined?(const_name, false)
          klass = parent.const_get(const_name, false)
        else
          klass = create_resource_subclass(superclass)
          parent.const_set const_name, klass

          klass.ontology_name = name
          klass.ontology_parent = parent

          parent.register_resource klass
        end

        klass.class_eval &block if block
      end
      alias_method :action, :resource

      def query(resource_name, &block)
        namespace :query do
          resource resource_name, ResourceQuery, &block
        end
      end

      private

      def create_resource_subclass(superclass)
        if superclass.is_a?(Symbol)
          superclass = const_get(constant_name(superclass))
        end
        Class.new(superclass)
      end

      def constant_name(ontology_name)
        Helpers.ontology_name_to_const_name(ontology_name)
      end
    end
  end
end
