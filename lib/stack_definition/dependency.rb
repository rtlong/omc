require 'stack_definition/heist_helpers'
require "stack_definition/resource_naming"
require "stack_definition/resource_definition"

module StackDefinition
  class Dependency
    include ResourceNaming

    def initialize(local_name, resource_class, resource_name)
      @local_name = HeistHelpers.name_from_heist(local_name)
      self.resource_class = resource_class
      self.resource_name = resource_name
    end

    attr_reader :local_name, :resource_class

    def lookup_resource(resource_collection)
      resource_collection.find_by_name(resource_full_name)
    end
  end
end
