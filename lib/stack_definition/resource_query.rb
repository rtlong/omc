require 'stack_definition/resource_ontology/ontology_attributes'

module StackDefinition
  class ResourceQuery
    class NoResultsError < StandardError
      def initialize(query)
        @query = query
      end
      attr_reader :query

      def to_s
        "Resource query for #{query.class} `#{query.query_hash.inspect}` returned no results!"
      end
    end

    class TooManyResultsError < NoResultsError
      def to_s
        "Resource query for #{query.class} `#{query.query_hash.inspect}` returned multiple results: #{query.results.inspect}"
      end
    end

    extend ResourceOntology::OntologyAttributes

    class <<self
      attr_reader :query_params
      attr_accessor :query_proc

      def query_params=(params)
        @query_params = Array(params)
        @query_params.each do |param_name|
          attr_accessor param_name
        end
      end
      alias_method :arguments, :query_params=

      def query_params
        @query_params || []
      end

      def query_via(query_class=nil, &block)
        if block_given?
          self.query_proc = block
        elsif query_class.respond_to?(:new)
          self.query_proc = -> (config, resource) do
            query_class.new(config).send(:"query_#{ontology_name}", resource)
          end
        else
          raise ArgumentError
        end
      end
    end

    def initialize(*query_values)
      if query_values.length != query_params.length
        fail ArgumentError.new(format("wrong number of arguments (%d for %d)",
                                      query_values.length,
                                      query_params.length))
      end

      query_params.zip(query_values).each do |(param, value)|
        self[param] = value
      end

      @results = [].freeze
    end

    attr_reader :results

    def only_result
      if results.empty?
        raise NoResultsError.new(self)
      elsif results.length > 1
        raise TooManyResultsError.new(self)
      end
      results.first
    end

    def reevaluate_with_scope(scope)
      query_params.each do |attr|
        self[attr] = HeistHelpers.evaluate_item_with_scope(self[attr], scope)
      end

      self
    end

    def [](key)
      self.public_send(key.to_sym)
    end

    def []=(key, value)
      methname = :"#{key}="
      respond_to?(methname) or raise UnknownAttributeError.new(key, self)
      query_params.include?(key) or raise SetterIsNotAttributeError.new(key, self)
      self.public_send(methname, value)
    end

    def query_params
      self.class.query_params
    end

    def query_hash
      Hash[query_params.map { |attr| [attr, self[attr]] }]
    end

    def accept(visitor)
      visitor.visit(self)
    end

    def perform(config)
      p = self.class.query_proc or
        return config.ui.warnf('No query handler defined for %s', resource_full_name)

      @results = Array(p.call(config, self.dup)).freeze
      self
    end

    def inspect
      %[(%s %s)] % [self.class.ontology_path, query_hash.inspect]
    end
  end
end
