require 'configured_instance'

module Queries
  class DigitalOcean
    include ConfiguredInstance

    def query_ssh_key(query)
      all_keys.select { |k| k.name == query[:name] }.map(&:id)
    end

    private

    def all_keys
      api.ssh_keys.all.to_a
    end

    def api
      config.digital_ocean_api
    end
  end
end
