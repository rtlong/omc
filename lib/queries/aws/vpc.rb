require 'configured_instance'

module Queries
  module AWS
    class VPC
      include ConfiguredInstance

      def query_vpc_id(query)
        filter = {
          name: 'tag:Name',
          values: [ query.name ]
        }
        ec2.describe_vpcs(filters: [filter])
          .vpcs
          .map(&:vpc_id)
      end

      def query_subnet_ids(query)
        filter = {
          name: 'vpc-id',
          values: [ query.vpc_id ]
        }

        ec2.describe_subnets(filters: [filter])
          .subnets
          .map(&:subnet_id)
      end

      def query_security_group_id(query)
        vpc_filter = {
          name: 'vpc-id',
          values: [ query.vpc_id ]
        }
        name_filter = {
          name: 'tag:Name',
          values: [ query.name ]
        }

        ec2.describe_security_groups(filters: [name_filter, vpc_filter])
          .security_groups
          .map(&:group_id)
      end

      private

      def ec2
        config.aws_api.ec2
      end
    end
  end
end
