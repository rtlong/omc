require 'configured_instance'

module Queries
  module AWS
    class IAM
      include ConfiguredInstance

      def query_server_certificate_id(query)
        find_server_certificate_by_name(query.name).server_certificate_id
      end

      def query_server_certificate_arn(query)
        find_server_certificate_by_name(query.name).arn
      end

      private

      def iam
        config.aws_api.iam
      end

      # FYI Not-found and other server errors raise exceptions here, hence no error handling
      def find_server_certificate_by_name(name)
        iam
          .get_server_certificate(server_certificate_name: name)
          .server_certificate
          .server_certificate_metadata
      end
    end
  end
end
