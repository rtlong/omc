# require 'stack_resources'
require 'configured_instance'
require 'visit_macro'

module Queries
  class Visitor
    include ConfiguredInstance
    include VisitMacro

    self.ignore_undefined_visits = true

    visits Object do |obj|
      obj
    end

    visits StackDefinition::ResourceDeclaration do |rd|
      rd.resource = rd.resource.accept(self)
    end

    visits StackDefinition::ResourceDefinition do |resource|
      resource.dup.tap do |copy|
        copy.each_attribute do |attr, value|
          next if value.nil?
          copy[attr] = value.accept(self)
        end
      end
    end

    visits Heist::Runtime::Vector do |v|
      v.map { |e| e.accept(self) }
    end

    visits StackDefinition::ResourceQuery do |q|
      ui.announce "Querying #{q.inspect}" do |announce|
        q.query_params.each do |param|
          q[param] = q[param].accept(self)
        end
        q.perform(config).only_result.tap { |result| announce.success(result) }
      end
    end
  end
end
