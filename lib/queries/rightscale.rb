require 'configured_instance'

module Queries
  class Rightscale
    include ConfiguredInstance

    def query_cloud_href(_)
      [cloud.href]
    end

    def query_network_href(query)
      rs
        .networks(filter: rs_filter(query.query_hash.merge({ cloud_href: cloud.href })))
        .index
        .map(&:href)
    end
    alias query_network_href_by_id query_network_href

    def query_instance_type_href(query)
      cloud
        .instance_types(filter: rs_filter(query.query_hash))
        .index
        .map(&:href)
    end

    def query_server_template_href(query)
      rs
        .server_templates(filter: rs_filter(query.query_hash))
        .index
        .select(&exact_name_matches(query.name))
        .map(&:href)
    end

    def query_ssh_key_href(query)
      cloud
        .ssh_keys(filter: rs_filter(query.query_hash))
        .index
        .map(&:href)
    end

    def query_security_group_href(query)
      cloud
        .security_groups(filter: rs_filter(query.query_hash))
        .index
        .select(&exact_name_matches(query.name))
        .map(&:href)
    end

    def query_datacenter_href(query)
      cloud
        .datacenters(filter: rs_filter(query.query_hash))
        .index
        .select(&exact_name_matches(query.name))
        .map(&:href)
    end

    def query_deployment_href(query)
      rs
        .deployments(filter: rs_filter(query.query_hash))
        .index
        .select(&exact_name_matches(query.name))
        .map(&:href)
    end

    def query_right_script_href(query)
      rs
        .right_scripts(filter: rs_filter(name: query.name))
        .index
        .select(&exact_name_matches(query.name))
        .select { |rightscript|
          target_rev = query.revision == 'HEAD' ? 0 : query.revision
          rightscript.revision == target_rev
        }.map(&:href)
    end

    def query_multi_cloud_image_href(query)
      rs
        .multi_cloud_images(filter: rs_filter(name: query.name))
        .index
        .select(&exact_name_matches(query.name))
        .select { |image|
          target_rev = query.revision == 'HEAD' ? 0 : query.revision
          image.revision == target_rev
        }.map(&:href)
    end

    private

    def rs
      config.rightscale_api
    end

    def rs_filter(query_hash)
      query_hash.map{|p| p.join('==') }
    end

    # Rightscale "name" filters often do substring matching, and many names for
    # which we might query are in fact substrings of other resources' names.
    def exact_name_matches(name)
      -> (result) { result.name == name }
    end

    def cloud
      clouds = rs.clouds(filter: rs_filter(name: config.stack_config.aws_region)).index
      clouds.length == 1 or fail "RS Cloud query for stack_config[aws_region] == `#{config.stack_config.aws_region}` failed"
      clouds.first
    end
  end
end
