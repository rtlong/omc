require 'stack_resources'
require 'stack_definition/runtime'
require 'configured_instance'
require 'helpers'
require 'queries/visitor'

module Commands
  class LaunchNewStack
    include ConfiguredInstance
    include Helpers::JSON

    def initialize(*)
      super

      config.stack_definition_file = config.pathname_if_readable_file(config.argv.shift) or
        ui.fail 'ERROR: Pass filename of Stack definition to launch.'
    end

    def run
      eval_and_verify_stack_definition
      launch
    rescue => ex
      ui.fatalf "%s: %s\n%s", ex.class, ex.message, ex.backtrace[0, 10].map{ |l| l.sub(%r{^/.+/gems/}, 'gems/') }.join("\n")
      ui.pry
      raise
    end

    def eval_and_verify_stack_definition
      ui.announce 'Eval and verification' do |announce_group|
        ui.announce 'Parsing stack definition' do |announce|
          stack_resources
        end

        ui.announce 'Validating dependency references' do |announce|
          stack_resources.validate_dependency_references!
        end

        ui.announce 'Validating deferred evaluations' do |announce|
          stack_runtime.with_new_scope do |scope|
            stack_resources.validate_deferred_evaluations!(config, scope)
          end
        end

        if ui.debug?
          ui.announce 'Determining execution order for safe serial execution' do |announce|
            stack_resources.each_nonprovisioned do |r|
              ui.infof '  - %s', r.resource_full_name
            end
          end
        end
      end
    end

    def resolve_queries
      ui.announce 'Resolving queries' do |announce|
        stack_resources.accept Queries::Visitor.new(config)
      end
    end

    def launch
      ui.announce 'Launching' do |announce_group|
        resolve_queries

        ui.announce 'Launching resources...' do |announce|
          stack_resources.each_nonprovisioned do |resource|
            stack_runtime.with_new_scope do |scope|
              ui.announce("Launching #{resource.resource_full_name}") {
                resource.launch(scope, config)
              }
            end
          end
        end
      end
    end

    def stack_resources
      stack_runtime.resources_collection
    end

    def stack_runtime
      @runtime ||= StackDefinition::Runtime.new(config, StackResources.definitions)
        .parse(config.stack_definition_file)
    end
  end
end
