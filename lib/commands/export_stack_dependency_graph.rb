require 'commands/launch_new_stack'

module Commands
  class ExportStackDependencyGraph < LaunchNewStack
    def initialize(*)
      super

      @output_filename = config.argv.shift or
        ui.fail 'ERROR: Pass filename for .dot file output'
    end

    attr_reader :output_filename

    def run
      eval_and_verify_stack_definition

      File.open(output_filename, 'w') do |file|
        file.puts "digraph G {"
        stack_resources.each_nonprovisioned do |r|
          file.printf %{  "%s"\n}, r.resource_full_name
          r.dependencies.each do |dep|
            file.printf %{  "%s" -> "%s"\n}, r.resource_full_name, dep.resource_full_name
          end
        end
        file.puts "}"
      end
    end
  end
end
