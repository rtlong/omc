require 'commands/launch_new_stack'
require 'core_ext/to_scm'

module Commands
  class ExportIntermediateRepresentation < LaunchNewStack
    include ConfiguredInstance
    include Helpers::JSON

    def initialize(*)
      super

      @output_filename = config.argv.shift or
        ui.fail 'ERROR: Pass filename for file output'
    end

    def run
      eval_and_verify_stack_definition
      resolve_queries
      export_ir(@output_filename)

    rescue => ex
      ui.fatalf "%s: %s\n%s", ex.class, ex.message, ex.backtrace[0, 10].map{ |l| l.sub(%r{^/.+/gems/}, 'gems/') }.join("\n")
      ui.pry
      raise
    end

    def export_ir(output_filename)
      File.open(output_filename, 'w') do |file|
        ui.announce 'Exporting IR' do |announce|
          file.puts config.stack_config.to_scm
          file.puts

          stack_resources.each_nonprovisioned do |resource_declaration|
            ui.pry
            deps = resource_declaration.dependencies.map { |d|
              ln = d.local_name ? d.local_name.to_sym : nil
              els = ['dep', ln.to_scm, d.resource_class.ontology_path, d.resource_name.to_scm]
              "(#{els.join(' ')})"
            }

            if deps.empty?
              deps = "'()"
            else
              deps = "(list\n  #{deps.join("\n  ")})"
            end
            resource = resource_declaration.resource.to_scm

            file.puts "(register-resource\n %s)" % [resource_declaration.resource_name.to_scm, deps, resource].join("\n ")
            file.puts
          end
        end
      end
    end

    def stack_resources
      stack_runtime.resources_collection
    end

    def stack_runtime
      @runtime ||= StackDefinition::Runtime.new(config, StackResources.definitions)
        .parse(config.stack_definition_file)
    end
  end
end
