class Object
  def accept(visitor)
    visitor.visit(self)
  end
end
