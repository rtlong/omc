class Object
  def to_scm
    inspect
  end
end

class TrueClass
  def to_scm
    '#t'
  end
end

class FalseClass
  def to_scm
    '#f'
  end
end

class NilClass
  def to_scm
    "'()"
  end
end

class Array
  def to_scm
    return nil.to_scm if empty?
    "(vector\n#{map(&:to_scm).join("\n")})"
  end
end

class Symbol
  def to_scm
    "'#{to_s}"
  end
end

class Heist::Runtime::Cons
  def to_scm
    "`#{to_s}"
  end
end
