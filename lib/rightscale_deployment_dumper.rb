class RightscaleDeploymentDumper
  NicknameCollision = Class.new(StandardError)

  def initialize(client:, deployment_id:)
    @deployment_id = deployment_id
    @client = client
  end

  attr_reader :client

  def deployment
    @deployment ||= @client.deployments(id: @deployment_id).show
  end

  def dump_inputs(resource_with_inputs)
    resource_with_inputs.inputs.index.inject({}) do |hash, input|
      hash[input.name] = dump_input_value(input.value)
      hash
    end
  end

  def dump_input_value(value)
    type, value = value.split(':', 2)
    dumped = { type: type }
    case type
    when 'array'
      dumped[:value] = value.split(',')
    when 'key'
      value, index = value.split(':')
      dumped[:value] = value
      dumped[:index] = index
    else
      dumped[:value] = value
    end
    dumped
  end

  def lookup_resource_name(resource_href)
    resource = @client.resource(resource_href)
    resource.name
  end

  # expects an array of Hashes with 'href' keys
  def dump_security_groups(security_groups)
    security_groups.inject([]) do |groups, sg|
      groups << {
        href: sg['href'],
        name: lookup_resource_name(sg['href']),
      }
      groups
    end
  end

  def dump_instance(instance)
    {
      cloud_specific_attributes: instance.cloud_specific_attributes,
      instance_type: {
        name: instance.instance_type.show.name,
        href: instance.instance_type.href,
      },
      image: {
        name: instance.image.show.name,
        href: instance.image.href,
      },
      multi_cloud_image: {
        name: instance.multi_cloud_image.show.name,
        href: instance.multi_cloud_image.href,
      },
      ssh_key: {
        resource_uid: instance.ssh_key.show.resource_uid,
        href: instance.ssh_key.href,
      },
      security_groups: dump_security_groups(instance.security_groups),
      template: {
        name: instance.server_template.show.name,
        href: instance.server_template.href,
      },
      inputs: inputs_changed_from_deployment(dump_inputs(instance)),
    }
  end

  def deployment_inputs
    @deployment_inputs ||= dump_inputs(deployment)
  end

  def filter_only_changed(parent_inputs, child_inputs)
    child_inputs.inject({}) do |hash, (name, child_value)|
      hash[name] = child_value if parent_inputs[name] != child_value
      hash
    end
  end

  def inputs_changed_from_deployment(child_inputs)
    filter_only_changed(deployment_inputs, child_inputs)
  end

  def dump_server(server)
    puts "Fetching configuration for Server: #{server.name}"
    {
      name: server.name,
      description: server.description,
      next_instance: dump_instance(server.next_instance.show(view: 'full_inputs_2_0')),
      tags: dump_tags(server),
    }
  end

  def dump_datacenter_policy(datacenter_policy)
    datacenter_policy.map { |policy_rule|
      policy_rule.merge(
        datacenter_name: lookup_resource_name(policy_rule['datacenter_href']),
      )
    }
  end

  def dump_server_array(server_array)
    puts "Fetching configuration for Server Array: #{server_array.name}"
    {
      name: server_array.name,
      description: server_array.description,
      array_type: server_array.array_type,
      datacenter_policy: dump_datacenter_policy(server_array.raw.fetch('datacenter_policy', [])),
      elasticity_params: server_array.raw.fetch('elasticity_params', {}),
      next_instance: dump_instance(server_array.next_instance.show(view: 'full_inputs_2_0')),
      tags: dump_tags(server_array),
    }
  end

  def resource_nickname_from_fqdn(fqdn)
    if (deployment_domain = deployment_inputs['DOMAIN'])
      deployment_domain = deployment_domain[:value]
    else
      deployment_domain = deployment.name
    end
    fqdn.gsub(/\.#{deployment_domain}/, '')
  end

  def dump_tags(resource)
    tags = @client.tags.by_resource(resource_hrefs: [resource.href]).first.tags
    tags.delete_if { |tag|
      /\A(rs_launch:|gg:env|app|worker|network)=/.match(tag.to_s)
    }
  end

  def dump_deployment
    output_document = {}

    output_document[:deployment] = {
      name:             deployment.name,
      description:      deployment.description,
      server_tag_scope: deployment.server_tag_scope,
      inputs:           deployment_inputs,
      tags:             dump_tags(deployment),
    }

    output_document[:servers] = deployment.servers.index.inject({}) { |servers, server|
      nick = resource_nickname_from_fqdn(server.name)
      fail(NicknameCollision, nick) if servers.key?(nick)
      servers[nick] = dump_server(server)
      servers
    }

    output_document[:server_arrays] = deployment.server_arrays.index.inject({}) { |arrays, server_array|
      nick = resource_nickname_from_fqdn(server_array.name)
      fail(NicknameCollision, nick) if arrays.key?(nick)
      arrays[nick] = dump_server_array(server_array)
      arrays
    }

    output_document
  end
end
