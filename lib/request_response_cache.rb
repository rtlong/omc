require 'set'
require 'digest'

require 'cache'


class RequestResponseCache
  def initialize(ui)
    @ui = ui
    @cache = Cache.new
    @url_index = Hash.new { |h, url| h[url] = Set.new }
  end

  attr_accessor :cache

  # has same semantics of Hash#fetch
  def fetch(url, method, headers, &block)
    key = cache_key(url, method, headers)
    @url_index[url] << key
    @cache.fetch(key) do
      @ui.debug "[Request-Cache:MISS] #{method} #{url}"
      block.call.map { |value|
        if value.is_a?(RestClient::AbstractResponse)
          value = value + '' # force allocation of new string with value copied into it inorder to drop the singleton components of the response object
        end
        value
      }
    end
  end

  def clear
    @cache.clear
  end

  def clear_url(url)
    @url_index[url].each do |key|
      @cache.delete key
    end
  end

  private

  def cache_key(url, method, headers)
    key = []
    key << url
    key << method.to_s
    # case url
    # when %r[/api/session\z]
    # else
    #   p headers
    #   key << Digest::SHA256.hexdigest(headers.inspect)
    # end
    key.join(':')
  end

end
