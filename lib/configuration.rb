require 'pathname'

require 'redis'

require 'ui'
require 'caching_rest_client'
require 'request_response_cache'

class Configuration
  AWSAPI = Struct.new(
    :cloudfront,
    :dynamodb,
    :ec2,
    :elasticache,
    :elb,
    :iam,
    :rds,
    :cloudformation,
    :s3,
    :vpc,
  )

  def initialize(argv, env)
    @argv = parse_opts(argv)
    @env = env
    self.ui = UI.new
    @stack_config = StackConfig.new

    @request_cache = RequestResponseCache.new(ui)
    if redis_url
      ui.warn "Using REDIS_URL = #{redis_url}"
      @request_cache.cache = Cache.wrap(Redis.new(url: redis_url)).tap { |cache|
        cache.config.default_ttl = 3600
      }
    end
  end

  attr_reader(
    :ui,
    :dry_run,
    :argv,
    :stack_config,
    :aws_api,
    :rightscale_api,
    :dns_made_easy_api,
    :request_cache
  )

  attr_accessor :stack_definition_file, :debug

  def ui=(new_ui)
    @ui = new_ui
    @ui.debug = @debug
  end

  def pathname_if_readable_file(path)
    return false if path.nil?
    pathname = Pathname.new(path)
    return false unless pathname.file?
    pathname
  end

  def aws_api
    require 'aws-sdk'
    @aws_api ||= AWSAPI.new.tap { |aws|
      aws.cloudfront = Aws::CloudFront::Client.new
      aws.dynamodb = Aws::DynamoDB::Client.new
      aws.ec2 = Aws::EC2::Client.new
      aws.elasticache = Aws::ElastiCache::Client.new
      aws.elb = Aws::ElasticLoadBalancing::Client.new
      aws.iam = Aws::IAM::Client.new
      aws.rds = Aws::RDS::Client.new
      aws.cloudformation = Aws::CloudFormation::Client.new
      aws.s3 = Aws::S3::Client.new
    }
  end

  def rightscale_api
    require 'right_api_client'
    @rightscale_api ||= RightApi::Client.new(
      refresh_token: rightscale_refresh_token,
      account_id: rightscale_account_id,
      api_url: rightscale_api_endpoint,
      timeout: nil,
      rest_client_class: CachingRestClient::Factory.new(@request_cache),
    )
  end

  def dns_made_easy_api
    require 'dnsmadeeasy-rest-api'
    @dns_made_easy_api ||= DnsMadeEasy.new(
      dns_made_easy_api_token,
      dns_made_easy_api_secret,
      false)
  end

  def digital_ocean_api
    require 'droplet_kit'
    @digital_ocean_api ||= DropletKit::Client.new(access_token: digital_ocean_access_token)
  end

  private

  def parse_opts(argv)
    @debug = [argv.delete('-d'), argv.delete('--debug'), ENV['DEBUG']].compact.any?
    @dry_run = [argv.delete('-n'), argv.delete('--dry-run')].compact.any?
    argv
  end

  def dns_made_easy_api_secret
    must_fetch_env_var('DNSMADEEASY_API_SECRET')
  end

  def dns_made_easy_api_token
    must_fetch_env_var('DNSMADEEASY_API_TOKEN')
  end

  def rightscale_refresh_token
    must_fetch_env_var('RIGHTSCALE_REFRESH_TOKEN')
  end

  def rightscale_api_endpoint
    must_fetch_env_var('RIGHTSCALE_API_ENDPOINT')
  end

  def rightscale_account_id
    must_fetch_env_var('RIGHTSCALE_ACCOUNT_ID')
  end

  def digital_ocean_access_token
    must_fetch_env_var('DIGITALOCEAN_API_TOKEN')
  end

  def redis_url
    ENV['REDIS_URL']
  end

  def must_fetch_env_var(var)
    ENV.fetch(var) do
      warn "Set #{var}!"
      exit 1
    end
  end

  class StackConfig
    attr_accessor :aws_region

    def []=(variable_name, value)
      attributes << variable_name.to_sym
      self.public_send(:"#{variable_name}=", value)
    end

    def [](variable_name)
      self.public_send(variable_name)
    end

    def attributes
      @attributes ||= Set.new
    end

    def to_scm
      attributes.map { |attr|
        "(configure #{attr.to_scm} #{self[attr].to_scm})"
      }.join("\n")
    end
  end
end
