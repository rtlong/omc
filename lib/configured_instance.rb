module ConfiguredInstance
  def initialize(config)
    @config = config
  end

  attr_reader :config

  def ui
    config.ui
  end
end
