require 'time'
require 'ansi/code'
require 'helpers'
require 'logger'
require 'forwardable'

class UI
  extend Forwardable

  def initialize(stdin=$stdin, stdout=$stdout, stderr=$stderr)
    @stdin = stdin
    @stdout = stdout
    @stderr = stderr
    @debug = false
  end

  attr_writer :debug

  def debug?
    !!@debug
  end

  def logger
    return @logger if defined?(@logger)
    @logger = Logger.new(@stdout)
    @logger.level = debug? ? Logger::DEBUG : Logger::INFO
    @logger.formatter = ANSILogFormatter.new
    @logger
  end

  def_delegators :logger, :debug, :error, :info, :warn, :fatal

  def announce(activity_str, &block)
    Announcement.new(self, activity_str).go(&block)
  end


  def logf(loglevel, fstr, *args)
    self.public_send(loglevel, format(fstr, *args))
  end

  [:info, :debug, :error, :warn, :fatal].each do |log_method|
    define_method(:"#{log_method}f") do |fstr, *args|
      logf(log_method, fstr, *args)
    end
  end

  def pry
    return unless defined?(Pry)
    return warn("refusing to pry without TTY") unless STDIN.tty? and STDOUT.tty?
    binding.of_caller(1).pry
  end

  def fail(message=nil, exitstatus=1)
    fatal(message) if message
    self.exit(exitstatus)
  end

  def exit(exitstatus=1)
    Kernel.exit exitstatus
  end

  class Announcement < Struct.new(:ui, :name)
    include Helpers::Formatting

    def start(str='begin')
      raise 'Already started' if defined?(@start)
      ui.infof('%s [%s]', name, str)
      @start = Time.now
    end

    def success(reason=nil)
      finalize('success', reason)
    end

    def fail(reason=nil)
      finalize('fail', reason, false)
    end

    def finalize(result, reason, success=true)
      return if defined?(@finalized)
      @finalized = true

      loglevel = success ? :info : :error
      reason_str = reason ? " -- #{reason}" : ''
      ui.logf(loglevel, "%s [%s] (%s)%s", name, result, format_duration(Time.now - @start), reason_str)
    end

    def go(&block)
      start
      begin
        retval = block.call(self)
        success
        return retval
      rescue
        self.fail
        raise
      end
    end
  end

  class ANSILogFormatter
    def call(severity, datetime, progname, msg)
      s = ansi_color_for_severity(severity)
      s << datetime.iso8601(6)
      s << ' | %5s | ' % severity
      s << "[#{progname}] | " if progname
      s << clean_message_of_resets(msg, severity)
      s << "\n"
      s << ANSI.reset
    end

    def ansi_color_for_severity(severity)
      case severity
      when "DEBUG"
        ANSI.blue
      when "INFO"
        ANSI.green
      when "WARN"
        ANSI.magenta
      when "ERROR"
        ANSI.red
      when "FATAL"
        ANSI.red_on_black
      end
    end

    def clean_message_of_resets(msg, severity)
      msg.gsub(ANSI.reset, ANSI.reset << ansi_color_for_severity(severity))
    end
  end
end
