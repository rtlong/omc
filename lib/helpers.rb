require 'oj'

module Helpers
  module JSON
    def json_dump(obj)
      Oj.dump(obj, mode: :compat, indent: 2)
    end

    def json_parse(str)
      Oj.load(str, mode: :compat)
    end

    def try_json_parse(str)
      json_parse(str)
    rescue
      nil
    end
  end

  module Formatting
    def indent(str, level=2)
      s = ""
      indent = " " * level
      str.each_line do |line|
        s << indent
        s << line
      end
      s
    end

    def format_duration(duration)
      if duration < 1
        format('%dms', duration * 1_000)
      else
        format('%0.2fs', duration)
      end
    end
  end
end
