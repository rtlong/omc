require 'configured_instance'
require 'helpers'
require 'visit_macro'

module Launchers
  class DigitalOcean
    include ConfiguredInstance
    include Helpers::Formatting

    def launch_droplet(resource)
      ui.announce "Launching new droplet for DigitalOcean resources: #{resource.resource_name}" do |announce|
        return announce.success 'skip' if config.dry_run

        p resource.to_hash
        droplet = DropletKit::Droplet.new(resource.to_hash)
        api.droplets.create(droplet)
      end
      resource.provisioned = true
    end

    private

    def api
      config.digital_ocean_api
    end
  end
end
