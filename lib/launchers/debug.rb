require 'configured_instance'

module Launchers
  class Debug
    include ConfiguredInstance

    def launch_pp(resource)
      ui.debugf "[debug/pp %s]: %s", resource.resource_name, resource.value.inspect
      resource.provisioned = true
    end
  end
end
