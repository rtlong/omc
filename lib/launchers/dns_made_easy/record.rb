require 'helpers'
require 'configured_instance'

module Launchers
  module DnsMadeEasy
    class Record
      include ConfiguredInstance
      include Helpers::Formatting

      Record = Struct.new(:zone, :id, :name, :type, :value, :ddns, :ttl) do
        def self.from_api(zone, api_response_hash)
          new(zone, *api_response_hash.values_at(*%w[id name type value dynamicDns ttl]))
        end

        def api_args
          [name, type, value, api_options]
        end

        def api_options
          {
            'dynamicDns' => ddns,
            'ttl' => ttl,
          }
        end

        def ddns
          !!self[:ddns]
        end

        def hash
          [name, value, type].hash
        end

        def eql?(other)
          hash == other.hash
        end

        def to_s
          to_h.to_s
        end
      end

      def launch_dns_record(resource)
        existing_records = find_existing_records(resource)
        desired_records = build_desired_records(resource)

        convergable_records = existing_records & desired_records
        obsolete_records = existing_records - convergable_records
        records_to_create = desired_records - existing_records

        delete_obsolete_records(obsolete_records)
        converge_records(convergable_records, desired_records)
        new_records = create_new_records(records_to_create)

        unless config.dry_run
          resource.ids = new_records.map(&:id) + convergable_records.map(&:id)
        end
        resource.provisioned = true
      rescue Net::HTTPServerException => ex
        ui.pry or ui.fail format("%d %s\n%s",
                                 ex.response.code,
                                 ex.response.message,
                                 indent(ex.response.body, 4))
      rescue => ex
        ui.pry or raise
      end

      private

      def delete_obsolete_records(obsolete_records)
        obsolete_records.each do |record|
          announce "Deleting obsolete DNS record #{record}" do |announce|
            api.delete_record(obsolete_records.first.zone, record.id)
          end
        end
      end

      def converge_records(convergable_records, desired_records)
        convergable_records.each do |record|
          desired_record = desired_records.detect { |dr| dr.eql?(record) } or raise "WTF"
          converge_record record, desired_record
        end
      end

      def converge_record(existing_record, desired_record)
        announce "Converging DNS record #{desired_record}" do |announce|
          if desired_record.api_args == existing_record.api_args
            announce.success 'already exists'
          else
            api.update_record existing_record.zone, existing_record.id, *desired_record.api_args
          end
        end
      end

      def create_new_records(records_to_create)
        records_to_create.map do |record|
          announce "Creating DNS record #{record}" do |announce|
            api_response = api.create_record(record.zone, *record.api_args)
            Record.from_api(record.zone, api_response)
          end
        end
      end

      def build_desired_records(resource)
        values = resource.values
        values << '0.0.0.0' if values.empty? && resource.ddns
        values.map { |value|
          Record.new(resource.zone, nil, resource.name, resource.type,
                     value, resource.ddns, resource.ttl)
        }
      end

      def find_existing_records(resource)
        current_records = api.records_for(resource.zone).fetch('data').map { |api_response|
          Record.from_api(resource.zone, api_response)
        }
        current_records.select { |record|
          record.name == resource.name && record.type == resource.type
        }
      end

      def announce(message, &block)
        ui.announce(message) do |announce|
          config.dry_run ? announce.success('skip') : block.call(announce)
        end
      end

      def api
        config.dns_made_easy_api
      end
    end
  end
end
