require 'configured_instance'
require 'helpers'

module Launchers
  module Rightscale
    class Deployment
      include ConfiguredInstance
      include Helpers::Formatting

      def launch_deployment(deployment)
        ui.announce "Creating RightScale Deployment #{deployment.name}" do |announce|
          if config.dry_run
            announce.success 'skip'
            deployment.provisioned = true
            next
          end

          begin
            if (deployment.href = find_existing(deployment.name))
              announce.success 'already exists'
            else
              res = api.deployments.create(deployment: deployment_attrs(deployment))
              deployment.href = res.href
              announce.success 'created'
            end
            deployment.provisioned = true
          rescue RightApi::ApiError => ex
            ui.pry
            raise
          end
        end
      end

      private

      def api
        config.rightscale_api
      end

      def deployment_attrs(deployment)
        {
          name: deployment.name,
          description: deployment.description,
          server_tag_scope: deployment.server_tag_scope,
        }
      end

      # FIXME: Having to refer to StackResources here is kinda gross...
      def find_existing(name)
        StackResources::Rightscale::Query::DeploymentHref.new(name).perform(config).results.first
      end

    end
  end
end
