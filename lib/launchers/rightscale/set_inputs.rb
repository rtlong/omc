require 'configured_instance'
require 'helpers'

module Launchers
  module Rightscale
    class SetInputs
      include ConfiguredInstance
      include Helpers::Formatting

      def launch_set_inputs_on_deployment(resource)
        ui.announce "Setting Input values for RightScale deployment #{resource.deployment_href}" do |announce|
          if config.dry_run
            resource.provisioned = true
            announce.success 'skip'
            next
          end

          begin
            deployment = api.resource(resource.deployment_href)
            deployment.inputs.multi_update(inputs: inputs_as_hash(resource.inputs))
          rescue RightApi::ApiError => ex
            ui.pry
            raise
          end
        end
        resource.provisioned = true
      end

      private

      def api
        config.rightscale_api
      end

      def inputs_as_hash(inputs)
        inputs.map { |i| [i.name, i.full_value] }.to_h
      end
    end
  end
end
