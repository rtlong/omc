require 'configured_instance'
require 'helpers'

module Launchers
  module Rightscale
    class Executable
      include ConfiguredInstance
      include Helpers::Formatting

      FINISHED_PATTERN = %r[\A(completed|failed):\s+]

      def launch_run_executable(resource)
        ui.announce "Running executable #{resource.resource_name}" do |announce|
          if config.dry_run
            resource.provisioned = true
            announce.success 'skip'
            next
          end

          begin
            instance = api.resource(resource.instance_href)
            task = instance.run_executable(run_config(resource))
            task = wait_task(task, resource.timeout || 3600)

            m = FINISHED_PATTERN.match(task.summary)
            unless m[1] == 'completed'
              announce.fail "Task failed #{task.show.summary}"
              return
            end
            resource.task = task
          rescue RightApi::ApiError => ex
            ui.pry
            raise
          end
        end
        resource.provisioned = true
      end

      private

      def api
        config.rightscale_api
      end

      def run_config(resource)
        h = {
          right_script_href: resource.right_script_href,
          ignore_lock: !!resource.ignore_lock
        }
        h[:inputs] = inputs_as_hash(resource.inputs) if resource.inputs.any?
        h
      end

      def inputs_as_hash(inputs)
        inputs.map { |i| [i.name, i.full_value] }.to_h
      end

      def wait_task(task, timeout)
        timeout_time = Time.now + timeout

        task_details = task.show
        until FINISHED_PATTERN === task_details.summary
          raise Timeout::TimeoutError if Time.now > timeout_time
          sleep 5
          task_details = task.show
        end

        task_details
      end
    end
  end
end
