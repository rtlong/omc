require 'configured_instance'
require 'helpers'
require 'visit_macro'

module Launchers
  module Rightscale
    class Servers
      include ConfiguredInstance
      include Helpers::Formatting

      def launch_server(resource)
        ui.announce "Creating Rightscale server definition #{resource.name}" do |announce|
          if config.dry_run
            announce.success 'skip'
            resource.provisioned = true
            next
          end

          begin
            deployment = deployment_for(resource)
            response = deployment.servers(filter: ["name==#{resource.name}"]).index.detect{|r| r.name == resource.name }
            response ||= deployment.servers.create(server: server_config(resource))
            resource.href = response.href
          rescue => ex
            ui.pry or raise
          end
        end
        resource.provisioned = true
      end

      def launch_server_array(resource)
        ui.announce "Creating Rightscale server array definition #{resource.name}" do |announce|
          if config.dry_run
            announce.success 'skip'
            resource.provisioned = true
            next
          end

          begin
            deployment = deployment_for(resource)
            response = deployment.server_arrays(filter: ["name==#{resource.name}"]).index.detect{|r| r.name == resource.name }
            response ||= deployment.server_arrays.create(server_array: server_array_config(resource))
            resource.href = response.href
          rescue => ex
            ui.pry or raise
          end
        end
        resource.provisioned = true
      end

      def launch_enable_server_array(resource)
        ui.announce "Enabling Rightscale ServerArray #{resource.resource_name}" do |announce|
          if config.dry_run
            announce.success 'skip'
            resource.provisioned = true
            next
          end

          begin
            array = api.resource(resource.array_href)
            array.update(server_array: { state: 'enabled' })
          rescue => ex
            ui.pry or raise
          end
        end
        resource.provisioned = true
      end

      private

      def api
        config.rightscale_api
      end

      def deployment_for(resource)
        api.resource(resource.deployment_href)
      end

      def server_config(server)
        {
          name: server.name,
          description: server.description,
          instance: server_instance_config(server.instance),
        }
      end

      def server_array_config(array)
        formatter = Formatter.new
        {
          name: array.name,
          description: array.description,
          instance: server_instance_config(array.next_instance),
          array_type: array.array_type,
          datacenter_policy: array.datacenter_policy.accept(formatter),
          elasticity_params: array.elasticity_params.accept(formatter),
          state: 'disabled',
        }
      end

      def cloud_href
        Queries::Rightscale.new(config).query_cloud_href(nil).first
      end

      def server_instance_config(instance)
        formatter = Formatter.new
        Hash.new.tap do |h|
          instance.each_attribute do |attr, value|
            next if value.nil?
            if attr == :inputs
              val = inputs_as_hash(value)
            else
              val = value.accept(formatter)
            end
            h[attr] = val
          end
          h[:cloud_href] = cloud_href
        end
      end

      def inputs_as_hash(inputs)
        inputs.map { |i| [i.name, i.full_value] }.to_h
      end

      class Formatter
        include VisitMacro

        visits Object do |obj|
          obj
        end

        visits StackDefinition::ResourceDefinition do |resource|
          Hash.new.tap do |h|
            resource.each_attribute do |attr, value|
              next if value.nil?
              h[attr] = value.accept(self)
            end
          end
        end

        visits Enumerable do |v|
          v.map { |e| e.accept(self) }
        end
      end
    end
  end
end
