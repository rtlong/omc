require 'configured_instance'
require 'helpers'

module Launchers
  module Rightscale
    class Wait
      include ConfiguredInstance
      include Helpers::Formatting

      def launch_wait_instance(resource)
        ui.announce "Waiting for instance to become available #{resource.resource_name}" do |announce|
          if config.dry_run
            resource.provisioned = true
            announce.success 'skip'
            next
          end

          begin
            resource.instance = wait_for(resource.timeout || 3600) {
              instance = api.resource(resource.instance_href)
              ui.fail "instance is `#{instance.state}`" if instance_failed?(instance.state)
              ui.debugf 'state = %s', instance.state
              instance.state == resource.desired_state and instance
            }
          rescue => ex
            ui.pry or raise
          end
        end
        resource.provisioned = true
      end

      def launch_wait_array(resource)
        array = api.resource(resource.array_href)
        ui.announce "Waiting for ServerArray to become available: #{array.name}" do |announce|
          if config.dry_run
            resource.provisioned = true
            announce.success 'skip'
            next
          end

          begin
            resource.array = wait_for(resource.timeout || 3600) {
              instances = array.current_instances.index
              if instances.all? { |i| instance_failed?(i.state) }
                ui.fail 'all instances have failed'
              end
              ui.debugf 'states = %s', instances.map { |i| [i.resource_uid, i.state] }.inspect
              count = instances.map(&:state).count(resource.desired_state)
              count >= (resource.min_instances_ready || 1) and array
            }
          rescue => ex
            ui.pry or raise
          end
        end
        resource.provisioned = true
      end

      def launch_wait_for_ip(resource)
        ui.announce "Waiting for instance to receive public IP address #{resource.resource_name}" do |announce|
          if config.dry_run
            resource.provisioned = true
            announce.success 'skip'
            next
          end

          begin
            resource.instance = wait_for(resource.timeout || 3600) {
              instance = api.resource(resource.instance_href)
              next unless instance.attributes.include?(:public_ip_addresses)
              instance.public_ip_addresses.any? and instance
            }
          rescue => ex
            ui.pry or raise
          end
        end
        resource.provisioned = true
      end

      private

      def api
        config.rightscale_api
      end

      def instance_failed?(instance_state)
        /stranded/ === instance_state
      end

      def wait_for(timeout=3600, interval=5, &block)
        timeout_time = Time.now + timeout

        until (retval = block.call)
          raise Timeout::TimeoutError if Time.now > timeout_time
          sleep interval
        end
        retval
      end
    end
  end
end
