require 'configured_instance'
require 'helpers'

module Launchers
  module Rightscale
    class Tags
      include ConfiguredInstance
      include Helpers::Formatting

      def launch_add_tags(resource)
        title = "Adding tags to RightScale resources #{resource.resource_hrefs.join(', ')}: #{resource.tags.map(&:name).join(', ')}"
        ui.announce title do |announce|
          if config.dry_run
            resource.provisioned = true
            announce.success 'skip'
            next
          end

          begin
            api.tags.multi_add(resource_hrefs: resource.resource_hrefs, tags: resource.tags.map(&:name))
          rescue RightApi::ApiError => ex
            ui.pry
            raise
          end
        end
        resource.provisioned = true
      end

      private

      def api
        config.rightscale_api
      end
    end
  end
end
