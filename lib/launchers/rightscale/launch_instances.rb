require 'configured_instance'
require 'helpers'
require 'visit_macro'

module Launchers
  module Rightscale
    class LaunchInstances
      include ConfiguredInstance
      include Helpers::Formatting

      class InstanceExistsInBadState < StandardError
        def initialize(instance)
          @instance = instance
        end

        def to_s
          format('Instance %s already exists but in a bad state: %s', @instance.resource_uid, @instance.state)
        end
      end

      def launch_launch_instance(resource)
        ui.announce "Launching new instance(s) for RightScale resources: #{resource.resource_name}" do |announce|
          if config.dry_run
            resource.provisioned = true
            announce.success 'skip'
            next
          end

          begin
            launchable = api.resource(resource.resource_href)
            instance = get_current_instance(launchable)
            instance ||= launchable.launch(config_for_launch(resource))
            resource.instance = instance
          rescue RightApi::ApiError => ex
            ui.pry
            raise
          end
        end
        resource.provisioned = true
      end

      private

      def api
        config.rightscale_api
      end

      def get_current_instance(launchable)
        return get_any_instance_from_array(launchable) if launchable.resource_type == 'server_array'
        return nil unless launchable.respond_to?(:current_instance)

        instance = launchable.current_instance.show
        return nil if instance.state == 'terminated'
        instance_is_going_up(instance) or raise InstanceExistsInBadState.new(instance)
        instance
      end

      def instance_is_going_up(instance)
        case instance.state
        when 'operational', 'booting', 'pending'
          true
        else
          false
        end
      end

      def get_any_instance_from_array(array)
        array.current_instances.index.detect { |instance|
          instance_is_going_up(instance)
        }
      end

      def config_for_launch(resource)
        {
          inputs: inputs_as_hash(resource.inputs)
        }
      end

      def inputs_as_hash(inputs)
        inputs.map { |i| [i.name, i.full_value] }.to_h
      end
    end
  end
end
