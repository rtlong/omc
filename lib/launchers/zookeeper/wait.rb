require 'configured_instance'
require 'helpers'

module Launchers
  module Zookeeper
    class Wait
      include ConfiguredInstance
      include Helpers::Formatting

      class NotYetReadyError < StandardError
        def message
          "This zookeeper node is not yet serving requests"
        end
      end

      def launch_wait_until_ready(resource)
        ui.announce "Waiting for Zookeeper instance to become ready #{resource.resource_name}" do |announce|
          return announce.success 'skip' if config.dry_run

          wait_zookeeper_instance(resource.address, resource.client_port, (resource.timeout || 3600))
        end
        resource.provisioned = true
      end

      private

      def wait_zookeeper_instance(endpoint, port, timeout=3600)
        wait_for(timeout) {
          begin
            sock = TCPSocket.new(endpoint, port)
            sock.write('srvr')
            resp = sock.read
            if /not currently serving requests/.match(resp)
              raise NotYetReadyError
            else
              return true
            end
          rescue SystemCallError, NotYetReadyError => ex
            p ex: ex
            next false
          ensure
            sock.close if sock && !sock.closed?
          end
        }
      end

      def wait_for(timeout=3600, interval=5, &block)
        timeout_time = Time.now + timeout

        until (retval = block.call)
          raise Timeout::TimeoutError if Time.now > timeout_time
          p retval: retval, time_left: (Time.now - timeout_time)
          sleep interval
        end
        retval
      end
    end
  end
end
