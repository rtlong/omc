require 'configured_instance'

module Launchers
  module AWS
    class CloudFormation
      include ConfiguredInstance

      VALID_STATES = {
        update: [
          'UPDATE_COMPLETE',
          'UPDATE_COMPLETE_CLEANUP_IN_PROGRESS',
          'UPDATE_IN_PROGRESS',
        ],
        create: [
          'CREATE_COMPLETE',
          'CREATE_IN_PROGRESS',
          'DELETE_IN_PROGRESS',
        ]
      }

      COMPLETE_STATES = {
        create: [
          'CREATE_COMPLETE',
          'ROLLBACK_COMPLETE',
          'CREATE_ROLLBACK_COMPLETE',
          'DELETE_COMPLETE',
        ],
        update: [
          'UPDATE_COMPLETE',
          'ROLLBACK_COMPLETE',
          'UPDATE_ROLLBACK_COMPLETE',
        ],
      }

      def launch_stack(resource)
        ui.announce "Creating AWS CloudFormation stack `#{resource.name}`" do |announce|
          if config.dry_run
            announce.success 'skip'
            resource.provisioned = true
            next
          end

          validate_template! resource

          begin
            current = current_stack(resource.name)
          rescue Aws::CloudFormation::Errors::ValidationError
            current = false
          end

          if current
            ui.announce 'update existing stack' do
              stack = update(resource)
              resource.stack_id = stack.stack_id
              resource.operation = :update
            end
          else
            stack = create(resource)
            resource.stack_id = stack.stack_id
            resource.operation = :create
          end
        end

        resource.provisioned = true
      end

      # FIXME: This didn't wait for a stack with a nested stack resource
      def launch_wait_stack(resource)
        ui.announce "Waiting for AWS CloudFormation stack `#{resource.resource_name}`" do |announce|
          if config.dry_run
            announce.success 'skip'
            resource.provisioned = true
            next
          end

          stack = nil

          wait_for(resource.timeout, 2) do
            curr = current_stack(resource.stack_id)
            ui.debugf 'current status: %s', curr.stack_status

            unless VALID_STATES.fetch(resource.operation.to_sym).include?(curr.stack_status)
              announce.fail "Stack entered failed state: #{curr.stack_status} -- will wait for cleanup to finish"
            end

            next unless COMPLETE_STATES.fetch(resource.operation.to_sym).include?(curr.stack_status)
            stack = curr
          end

          next unless stack

          resource.stack_outputs = stack_outputs_as_hash(stack.outputs)
        end

        resource.provisioned = true
      end

      private

      def stack_outputs_as_hash(stack_outputs_collection)
        stack_outputs_collection.reduce({}) { |hash, output|
          hash[output.output_key] = output.output_value
          hash
        }
      end

      def current_stack(name_or_id)
        api.describe_stacks(stack_name: name_or_id).stacks.first
      end

      def read_file_at_uri(uri)
        # n.b. file:// URIs here with "relative" paths (those without a third
        # '/' making them absolute) will resolve to a path relative to the
        # stack definition file
        path = Pathname.new(uri.hostname.to_s + uri.path.to_s)
            .expand_path(config.stack_definition_file.parent.realpath)

        unless path.readable? && path.file?
          raise "unable to resolve file URI #{uri} to readable file"
        end

        # parse-then-dump because Ruby's JSON handles JS-style comments in JSON
        JSON.pretty_generate(JSON.parse(path.read))
      end

      def stack_template_options(resource)
        uri = URI.parse(resource.template_url)
        case uri.scheme
        when 'file'
          { template_body: read_file_at_uri(uri) }
        else
          { template_url: resource.template_url }
        end
      end

      def validate_template!(resource)
        api.validate_template(stack_template_options(resource))
      end

      def update(resource)
        api.update_stack(stack_request_params(resource))
      rescue Aws::CloudFormation::Errors::ValidationError => ex
        raise unless ex.message =~ /no updates are to be performed/i
      end

      def create(resource)
        params = stack_request_params(resource).merge(
          tags: build_tags_collection(resource.tags),
        )
        params[:timeout_in_minutes] = resource.timeout_in_minutes if resource.timeout_in_minutes
        api.create_stack(params)
      end

      def stack_request_params(resource)
        params = {
          stack_name: resource.name,
          notification_arns: Array(resource.notification_arn),
          parameters: build_stack_parameters_collection(resource.parameters),
        }
        params[:on_failure] = resource.on_failure if resource.on_failure

        params.merge(stack_template_options(resource))
      end

      def build_tags_collection(tags)
        tags.map do |tag|
          { key: tag.name, value: tag.value }
        end
      end

      def build_stack_parameters_collection(parameters)
        parameters.map do |parameter|
          {
            parameter_key: parameter.name,
            parameter_value: parameter.value,
          }
        end
      end

      def api
        config.aws_api.cloudformation
      end

      def wait_for(timeout=3600, interval=5, &block)
        timeout_time = Time.now + timeout

        until (retval = block.call)
          raise Timeout::TimeoutError if Time.now > timeout_time
          sleep interval
        end
        retval
      end
    end
  end
end
