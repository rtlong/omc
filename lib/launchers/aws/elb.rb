require 'configured_instance'

module Launchers
  module AWS
    class ELB
      include ConfiguredInstance

      def launch_instance(resource)
        ui.announce "Creating AWS ELB instance `#{resource.name}`" do |announce|
          if config.dry_run
            announce.success 'skip'
            next
          end

          lb = create_new_lb(resource)
          resource.dns_name = lb.dns_name
        end

        ui.announce "Configure AWS ELB Health Check for `#{resource.name}`" do |announce|
          if config.dry_run
            announce.success 'skip'
            next
          end

          api.configure_health_check(load_balancer_name: resource.name, health_check: resource.health_check.to_hash)
        end

        resource.provisioned = true
      end

      private

      # For ELBs, a CreateLoadBalancer action will act idempotently if there is
      # already a load balancer with the same settings existing. If any settings
      # are different, it raises an Aws::ElasticLoadBalancing::Errors::DuplicateLoadBalancerName exception
      def create_new_lb(resource)
        api.create_load_balancer(load_balancer_config(resource))
      end

      def load_balancer_config(resource)
        h = {
          load_balancer_name: resource.name,
          listeners: resource.listeners.map(&:to_hash),
          availability_zones: resource.availability_zones.map(&:name),
          subnets: resource.subnet_ids.map(&:id),
          security_groups: resource.security_group_ids.map(&:id),
        }
        h[:scheme] = resource.scheme if resource.scheme
        h[:tags] = resource.tags.map(&:to_hash) if resource.tags.any?
        h
      end

      def api
        config.aws_api.elb
      end
    end
  end
end
