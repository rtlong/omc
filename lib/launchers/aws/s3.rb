require 'configured_instance'

module Launchers
  module AWS
    class S3
      include ConfiguredInstance

      def launch_sync_dir(resource)
        ui.announce "Syncing dir to s3 `#{resource.resource_name}`" do |announce|
          return announce.success 'skip' if config.dry_run

          dest = resolve_dest(resource)
          source = resolve_source(resource)

          each_source_file(source) do |fpath|
            dest_file_path = dest[:path].join(fpath.relative_path_from(source)).to_s

            source_file_path = fpath.relative_path_from(Pathname.getwd)
            dest_file_url = URI.join(resource.dest, dest_file_path)
            ui.debug "Transferring #{source_file_path} => #{dest_file_url}"

            response = s3.put_object(
              bucket: dest[:bucket],
              key: dest_file_path,
              body: fpath.read,
            )
          end
        end
        resource.provisioned = true
      end

      private

      def resolve_dest(resource)
        uri = URI.parse(resource.dest)
        case uri.scheme
        when 's3'
          return {
            bucket: uri.hostname,
            path: Pathname.new(remove_leading_slash(uri.path))
          }
        else
          fail "dest argument must be a URI with scheme 's3://'"
        end
      end

      def resolve_source(resource)
        uri = URI.parse(resource.source)
        case uri.scheme
        when 'file'
          # n.b. file:// URIs here with "relative" paths (those without a third
          # '/' making them absolute) will resolve to a path relative to the
          # stack definition file
          path = Pathname.new(uri.hostname.to_s + uri.path.to_s)
            .expand_path(config.stack_definition_file.parent.realpath)

          unless path.readable?
            fail "unable to resolve file URI #{uri} to readable path"
          end

          return path
        else
          fail "source argument must be a URI with scheme 'file://'"
        end
      end

      def each_source_file(pathname, &block)
        return block.call(pathname) if pathname.file?
        unless pathname.directory?
          ui.warn "Skipping #{pathname}, as it's not a file or directory"
          return
        end
        pathname.children.each { |e| each_source_file(e, &block) }
      end

      def s3
        config.aws_api.s3
      end

      def remove_leading_slash(path)
        path.gsub(/^\//, '')
      end
    end
  end
end
