require 'configured_instance'

module Launchers
  module AWS
    class CloudFrontDistribution
      include ConfiguredInstance

      def launch_distribution(resource)
        ui.announce "Creating AWS CloudFront Distribution `#{resource.resource_name}`" do |announce|
          if config.dry_run
            announce.success 'skip'
            resource.provisioned = true
            next
          end

          d = nil

          begin
            d = create_new_distribution(resource)
          rescue Aws::CloudFront::Errors::DistributionAlreadyExists => ex
            d = find_existing(resource) or raise ex
            announce.success 'already exists'
          end
          resource.domain_name = d.domain_name
          resource.provisioned = true
        end
      rescue => ex
        ui.pry
      end

      private

      def create_new_distribution(resource)
        api.
          create_distribution(distribution_config: distribution_config(resource)).
          distribution
      end

      def find_existing(resource)
        all_distributions.detect do |d|
          d.aliases.items == resource.cname_aliases
        end
      end

      def all_distributions
        api.list_distributions.distribution_list.tap{ |dl|
          raise "Distributions have been truncated but there's no logic to get next page" if dl.is_truncated
        }.items
      end

      def distribution_config(resource)
        {
          comment: resource.description,
          caller_reference: resource.description,
          enabled: true,
          aliases: doofy_array(resource.cname_aliases),
          origins: doofy_array(resource.origins.map { |origin| origin_config(origin) }),
          default_cache_behavior: default_cache_behavior_config(resource),
          logging: {
            enabled: false,
            include_cookies: false,
            bucket: '',
            prefix: '',
          },
          viewer_certificate: viewer_certificate(resource.viewer_certificate),
          default_root_object: '',
          cache_behaviors: doofy_array([]),
          price_class: 'PriceClass_All',
        }
      end

      def origin_config(origin)
        {
          id: origin.domain_name,
          domain_name: origin.domain_name,
          custom_origin_config: {
            http_port: origin.http_port,
            https_port: origin.https_port,
            origin_protocol_policy: origin.protocol_policy,
          },
          origin_path: ''
        }
      end

      def default_cache_behavior_config(resource)
        dcb = resource.default_cache_behavior
        {
          target_origin_id: dcb.target_origin_id || resource.origins.first.domain_name,
          forwarded_values: forwarded_values_config(dcb.forwarded_values),
          trusted_signers: doofy_array([]).merge(enabled: false),
          viewer_protocol_policy: dcb.viewer_protocol_policy,
          min_ttl: 60 * 60 * 24 * 90,
        }
      end

      def forwarded_values_config(forwarded_values)
        {
          query_string: forwarded_values.query_string,
          cookies: forward_cookies_config(forwarded_values.cookies),
          headers: doofy_array(forwarded_values.headers),
        }
      end

      def forward_cookies_config(cookies)
        h = {
          forward: cookies.forward,
        }
        if cookies.forward == 'whitelist'
          h[:whitelisted_names] = doofy_array(cookies.whitelisted_names)
        end
        h
      end

      def viewer_certificate(vc)
        h = {
          ssl_support_method: vc.ssl_support_method,
          minimum_protocol_version: vc.minimum_protocol_version,
        }
        if vc.cloud_front_default_certificate
          h[:cloud_front_default_certificate] = true
        else
          h[:iam_certificate_id] = vc.iam_certificate_id
        end
        h
      end

      def api
        config.aws_api.cloudfront
      end

      def doofy_array(collection)
        h = {
          quantity: collection.length,
        }
        h[:items] = collection if collection.length > 0
        h
      end
    end
  end
end
