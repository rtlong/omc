require 'configured_instance'
require 'mustache'

module Launchers
  class Mustache
    include ConfiguredInstance

    def launch_render(resource)
      ui.announce "Rendering Mustache template #{resource.resource_name}" do |announce|
        bindings = {}
        resource.bindings.each do |b|
          bindings[b.name] = b.value
        end
        resource.contents = ::Mustache.render(resource.template, bindings)
        resource.provisioned = true
      end
    end
  end
end
