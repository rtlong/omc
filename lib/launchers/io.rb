require 'configured_instance'
require 'open-uri'

module Launchers
  class IO
    include ConfiguredInstance

    def launch_get_uri(resource)
      ui.announce "Retrieving resource at URI: #{resource.uri}" do |announce|

        resource.contents = resolve_uri(resource.uri).read
        resource.provisioned = true
      end
    end

    private

    def retry_on_failure(how_many_more_times=2, wait=1, &block)
      block.call
    rescue
      raise if how_many_more_times == 0
      ui.debug('failed to retrieve. will retry')
      sleep wait
      return retry_on_failure(how_many_more_times - 1, wait, &block)
    end

    def read_file_at_uri(uri)
      # n.b. file:// URIs here with "relative" paths (those without a third
      # '/' making them absolute) will resolve to a path relative to the
      # stack definition file
      path = Pathname.new(uri.opaque)
        .expand_path(config.stack_definition_file.parent.realpath)

      unless path.readable? && path.file?
        ui.pry
        raise "unable to resolve file URI #{uri} to readable file"
      end

      path
    end

    def resolve_uri(uri)
      uri = URI.parse(uri)
      case uri.scheme
      when 'file'
        read_file_at_uri(uri)
      else
        retry_on_failure do
          open(uri.to_s)
        end
      end
    end

  end
end
