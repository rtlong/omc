require 'configured_instance'
require 'yaml'
require 'tempfile'

module Launchers
  module Validators
    class CoreOS
      include ConfiguredInstance

      def launch_validate_cloud_config(resource)
        ui.announce "Validating CoreOS Cloud-Config `#{resource.resource_name}`" do |announce|
          # return announce.success 'skip' if config.dry_run

          validate_yaml resource.contents
          validate_cloudinit resource.contents
        end
        resource.provisioned = true
      end

      def validate_yaml(contents)
        YAML.load(contents)
      end

      def validate_cloudinit(contents)
        f = Tempfile.open('cloud-config.yml')
        f.write(contents)
        f.close

        success = system('coreos-cloudinit', '--validate', '--from-file', f.path)
        success or fail 'Template is not valid'
      end
    end
  end
end
