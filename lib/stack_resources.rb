require 'stack_definition/resource_ontology'
require 'stack_definition/resource_query'
require 'launchers/dns_made_easy/record'
require 'launchers/aws/cloud_front_distribution'
require 'launchers/aws/elb'
require 'launchers/aws/cloud_formation'
require 'launchers/aws/s3'
require 'launchers/rightscale/deployment'
require 'launchers/rightscale/tags'
require 'launchers/rightscale/servers'
require 'launchers/rightscale/set_inputs'
require 'launchers/rightscale/launch_instances'
require 'launchers/rightscale/wait'
require 'launchers/rightscale/executable'
require 'launchers/zookeeper/wait'
require 'launchers/debug'
require 'launchers/io'
require 'launchers/mustache'
require 'launchers/digital_ocean'
require 'launchers/validators/coreos'
require 'queries/aws/iam'
require 'queries/aws/vpc'
require 'queries/rightscale'
require 'queries/digital_ocean'

module StackResources
  extend StackDefinition::ResourceOntology

  namespace :dns_made_easy do
    resource :dns_record do
      attribute :zone, type: String
      attribute :name, type: String
      attribute :type, type: String
      collection :values, type: String
      attribute :ddns, type: [TrueClass, FalseClass]
      attribute :allow_multiple_records, type: [TrueClass, FalseClass]
      attribute :ttl, type: Integer

      launch_via Launchers::DnsMadeEasy::Record

      attr_accessor :ids

      def validate!
        ddns and raise "DDNS is no longer supported. Set the Value of this record by using wait_for_ip"
      end

      def ids
        Array(@ids)
      end

      def id
        fail "Multiple IDs" if ids.length > 1
        ids.first
      end

      def fqdn
        [name, zone].join('.')
      end
    end
  end

  namespace :aws do
    namespace :vpc do
      query :vpc_id do
        arguments :name
        query_via Queries::AWS::VPC
      end

      # query :subnet_id do
      #   arguments :vpc_id, :name
      #   query_via Queries::AWS::VPC
      # end

      query :subnet_ids do
        arguments :vpc_id
        query_via Queries::AWS::VPC
      end

      query :security_group_id do
        arguments [:vpc_id, :name]
        query_via Queries::AWS::VPC
      end
    end

    resource :tag do
      attribute :name, type: String
      attribute :value, type: String
    end

    namespace :iam do
      query :server_certificate_id do
        arguments :name
        query_via Queries::AWS::IAM
      end

      query :server_certificate_arn do
        arguments :name
        query_via Queries::AWS::IAM
      end
    end

    namespace :cloud_formation do
      resource :parameter do
        attribute :name, type: String
        attribute :value, type: String
      end

      resource :stack do
        attribute :name, type: String
        attribute :template_url, type: String
        attribute :timeout_in_minutes, type: Integer
        attribute :notification_arn, type: String
        attribute :on_failure, type: String
        collection :tags, type: 'aws/tag'
        collection :parameters, type: 'aws/cloud_formation/parameter'

        attr_accessor :stack_id
        attr_accessor :operation

        launch_via Launchers::AWS::CloudFormation
      end

      action :wait_stack do
        attribute :stack_id, type: String
        attribute :operation, type: String
        attribute :timeout, type: Integer

        attr_writer :stack_outputs

        def stack_outputs
          @stack_outputs || {}
        end

        launch_via Launchers::AWS::CloudFormation
      end
    end

    namespace :cloudfront do
      resource :origin do
        attribute :domain_name
        attribute :origin_path
        attribute :http_port
        attribute :https_port
        attribute :protocol_policy
      end

      resource :forward_cookies do
        attribute :forward, type: String
        collection :whitelisted_names, type: String
      end

      resource :forwarded_values do
        attribute :query_string, type: [TrueClass, FalseClass]
        attribute :cookies, type: 'aws/cloudfront/forward_cookies'
        collection :headers, type: String
      end

      resource :default_cache_behavior do
        attribute :target_origin_id
        attribute :forwarded_values, type: 'aws/cloudfront/forwarded_values'
        attribute :viewer_protocol_policy, type: String
      end

      resource :viewer_certificate do
        attribute :cloud_front_default_certificate, type: [TrueClass, FalseClass]
        attribute :iam_certificate_id, type: [String, 'aws/iam/query/server_certificate_id']
        attribute :acm_certificate_arn, type: [String]
        attribute :ssl_support_method, type: [String]
        attribute :minimum_protocol_version, type: [String]
      end

      resource :distribution do
        attribute :description
        collection :cname_aliases
        collection :origins, type: 'aws/cloudfront/origin'
        attribute :default_cache_behavior, type: 'aws/cloudfront/default_cache_behavior'
        attribute :viewer_certificate, type: 'aws/cloudfront/viewer_certificate'

        launch_via Launchers::AWS::CloudFrontDistribution

        attr_accessor :domain_name
      end
    end

    resource :availability_zone do
      attribute :name
      def initialize(name)
        self[:name] = name
      end
      def validate!
        raise "Unknown availability zone #{name}" unless /\Aus-east-1[abcde]\z/.match(name)
      end
    end

    namespace :elb do
      resource :listener do
        attribute :protocol, type: String
        attribute :load_balancer_port, type: Integer
        attribute :instance_protocol, type: String
        attribute :instance_port, type: Integer
        attribute :ssl_certificate_id, type: [String, 'aws/iam/query/server_certificate_arn']
      end

      resource :health_check do
        attribute :target, type: String # path to request with each check
        attribute :interval, type: Integer
        attribute :timeout, type: Integer
        attribute :unhealthy_threshold, type: Integer
        attribute :healthy_threshold, type: Integer
      end

      resource :instance do
        attribute :name # validate: /\A[\w-]+\z/i
        collection :availability_zones, type: 'aws/availability_zone'
        collection :subnet_ids, type: ['aws/vpc/query/subnet_ids']
        collection :security_group_ids, type: ['aws/vpc/query/security_group_id']
        collection :listeners, type: 'aws/elb/listener'
        collection :tags, type: 'aws/tag'

        attribute :health_check, type: 'aws/elb/health_check'
        attribute :scheme, type: String

        launch_via Launchers::AWS::ELB

        attr_accessor :dns_name

        def ipv6_domain_name
          ['ipv6', dns_name].join('.')
        end

        def dual_stack_domain_name
          ['dualstack', dns_name].join('.')
        end
      end
    end

    namespace :s3 do
      action :sync_dir do
        attribute :source, type: String
        attribute :dest, type: String

        launch_via Launchers::AWS::S3
      end
    end

  end

  namespace :rightscale do
    query :cloud_href do
      query_via Queries::Rightscale
    end

    query :network_href do
      arguments :name
      query_via Queries::Rightscale
    end
    query :network_href_by_id do
      arguments :resource_uid
      query_via Queries::Rightscale
    end

    query :server_template_href do
      arguments [:name, :revision]
      query_via Queries::Rightscale
    end

    query :security_group_href do
      arguments [:name, :network_href]
      query_via Queries::Rightscale
    end

    query :ssh_key_href do
      arguments :resource_uid
      query_via Queries::Rightscale
    end

    query :datacenter_href do
      arguments :name
      query_via Queries::Rightscale
    end

    query :deployment_href do
      arguments :name
      query_via Queries::Rightscale
    end

    query :instance_type_href do
      arguments :name
      query_via Queries::Rightscale
    end

    query :placement_group_href do
      arguments :name
      query_via Queries::Rightscale
    end

    query :subnet_href do
      arguments :name
      query_via Queries::Rightscale
    end

    query :multi_cloud_image_href do
      arguments [:name, :revision]
      query_via Queries::Rightscale
    end

    query :machine_image_href do
      arguments :name
      query_via Queries::Rightscale
    end

    query :kernel_image_href do
      arguments :name
      query_via Queries::Rightscale
    end

    query :ramdisk_image_href do
      arguments :name
      query_via Queries::Rightscale
    end

    query :right_script_href do
      arguments [:name, :revision]
      query_via Queries::Rightscale

    end

    namespace :input do
      resource :base do
        attribute :name, type: String
        attribute :value, type: String

        def initialize(name, value)
          self.name = name
          self.value = value
        end

        def type
          self.class.ontology_name
        end

        def full_value
          [type, value].join(':')
        end
      end

      resource :text, :base
      resource :key, :base
      resource :cred, :base
      resource :array, :base do
        collection :value, type: String
        def full_value
          [type, value.map{|v| "text:#{v}"}.inspect].join(':')
        end
      end
    end

    resource :deployment do
      attribute :name
      attribute :description
      attribute :server_tag_scope

      launch_via Launchers::Rightscale::Deployment

      attr_accessor :href
    end

    resource :cloud_specific_attributes do
      attribute :automatic_instance_store_mapping
      attribute :ebs_optimized # FIXME: is this still valid???
      attribute :iam_instance_profile
      attribute :root_volume_performance
      attribute :root_volume_performance
      attribute :root_volume_size
      attribute :root_volume_type_uid
    end

    resource :server_instance do
      attribute :associate_public_ip_address
      attribute :cloud_href,
        type: [String, 'rightscale/query/cloud_href']
      attribute :cloud_specific_attributes,
        type: 'rightscale/cloud_specific_attributes'
      attribute :datacenter_href,
        type: [String, 'rightscale/query/datacenter_href']
      attribute :image_href,
        type: [String, 'rightscale/query/machine_image_href']
      collection :inputs,
        type: ['rightscale/input/base']
      attribute :instance_type_href,
        type: [String, 'rightscale/query/instance_type_href']
      attribute :ip_forwarding_enabled
      attribute :kernel_image_href,
        type: [String, 'rightscale/query/kernel_image_href']
      attribute :multi_cloud_image_href,
        type: [String, 'rightscale/query/multi_cloud_image_href']
      attribute :placement_group_href,
        type: [String, 'rightscale/query/placement_group_href']
      attribute :ramdisk_image_href,
        type: [String, 'rightscale/query/ramdisk_image_href']
      collection :security_group_hrefs,
        type: [String, 'rightscale/query/security_group_href']
      attribute :server_template_href,
        type: [String, 'rightscale/query/server_template_href']
      attribute :ssh_key_href,
        type: [String, 'rightscale/query/ssh_key_href']
      collection :subnet_hrefs,
        type: [String, 'rightscale/query/subnet_href']
      attribute :user_data
    end

    resource :server do
      attribute :name
      attribute :deployment_href,
        type: [String, 'rightscale/query/deployment_href']
      attribute :description
      attribute :instance, type: 'rightscale/server_instance'

      launch_via Launchers::Rightscale::Servers

      attr_accessor :href
    end

    resource :datacenter_policy_rule do
      attribute :datacenter_href,
        type: [String, 'rightscale/query/datacenter_href']
      attribute :weight
      attribute :max
    end

    resource :elasticity_bounds do
      attribute :min_count
      attribute :max_count
    end

    resource :alert_specific_params do
      attribute :voters_tag_predicate
      attribute :decision_threshold
    end

    resource :schedule_entry do
      attribute :day
      attribute :time
      attribute :min_count
      attribute :max_count
    end

    resource :elasticity_pacing do
      attribute :resize_calm_time
      attribute :resize_up_by
      attribute :resize_down_by
    end

    resource :elasticity_params do
      attribute :pacing, type: 'rightscale/elasticity_pacing'
      attribute :alert_specific_params, type: 'rightscale/alert_specific_params'
      attribute :bounds, type: 'rightscale/elasticity_bounds'
      collection :schedule_entries, type: 'rightscale/schedule_entry'
    end

    resource :server_array do
      attribute :name
      attribute :description
      attribute :deployment_href,
        type: [String, 'rightscale/query/deployment_href']
      attribute :next_instance, type: 'rightscale/server_instance'
      attribute :array_type
      collection :datacenter_policy, type: 'rightscale/datacenter_policy_rule'
      attribute :elasticity_params, type: 'rightscale/elasticity_params'

      launch_via Launchers::Rightscale::Servers

      attr_accessor :href
    end

    resource :tag do
      attribute :name

      def initialize(namespace, predicate=nil, value=nil)
        if predicate || value
          @name = format('%s:%s=%s', namespace, predicate, value)
        else
          @name = namespace
        end
        validate!
      end

      def validate!
        unless /\A([^=:]+|.+?:.+?=.+)\z/.match(name)
          raise ArgumentError.new("Tag does not look right: #{name}")
        end
      end
    end

    action :add_tags do
      collection :resource_hrefs,
        type: [String, StackDefinition::ResourceQuery]
      collection :tags, type: 'rightscale/tag'

      launch_via Launchers::Rightscale::Tags
    end

    action :set_inputs_on_deployment do
      attribute :deployment_href, type: String
      collection :inputs, type: 'rightscale/input/base'

      launch_via Launchers::Rightscale::SetInputs
    end

    action :launch_instance do
      # href of Server or ServerArray definition upon which to run the launch method
      attribute :resource_href, type: [String, StackDefinition::ResourceQuery]
      collection :inputs, type: 'rightscale/input/base'

      launch_via Launchers::Rightscale::LaunchInstances

      attr_reader :href
      def instance=(instance)
        @href = instance.href
      end
    end

    action :wait_for_ip do
      attribute :instance_href, type: String
      attribute :timeout, type: Integer

      launch_via Launchers::Rightscale::Wait

      attr_reader :public_ip_address
      attr_reader :private_ip_address
      def instance=(instance)
        @public_ip_address = instance.public_ip_addresses.first
        @private_ip_address = instance.private_ip_addresses.first
      end
    end

    action :wait_array do
      attribute :array_href, type: String
      attribute :desired_state, type: String
      attribute :timeout, type: Integer
      attribute :min_instances_ready, type: Integer

      launch_via Launchers::Rightscale::Wait

      attr_accessor :array
    end

    action :wait_instance do
      attribute :instance_href, type: String
      attribute :desired_state, type: String
      attribute :timeout, type: Integer

      launch_via Launchers::Rightscale::Wait

      attr_accessor :href
      def instance=(instance)
        @href = instance.href
      end
    end

    action :run_executable do
      attribute :instance_href, type: String
      attribute :ignore_lock, type: [TrueClass, FalseClass]
      attribute :right_script_href, type: [String, 'rightscale/query/right_script_href']
      attribute :wait, type: [TrueClass, FalseClass]
      attribute :timeout, type: Integer
      collection :inputs, type: ['rightscale/input/base']

      launch_via Launchers::Rightscale::Executable

      def task=(task)

      end
    end

    action :enable_server_array do
      attribute :array_href, type: String

      launch_via Launchers::Rightscale::Servers
    end
  end

  namespace :digital_ocean do
    query :ssh_key do
      arguments :name
      attr_accessor :id
      query_via Queries::DigitalOcean
    end

    resource :droplet do
      attribute :name, type: String
      attribute :region, type: String
      attribute :size, type: String
      attribute :image, type: [String]
      collection :ssh_keys, type: [String, 'digital_ocean/query/ssh_key']
      attribute :backups, type: [TrueClass, FalseClass]
      attribute :ipv6, type: [TrueClass, FalseClass]
      attribute :private_networking, type: [TrueClass, FalseClass]
      attribute :user_data, type: String

      launch_via Launchers::DigitalOcean
    end
  end

  namespace :zookeeper do
    action :wait_until_ready do
      attribute :address, type: String
      attribute :client_port, type: Integer
      attribute :timeout, type: Integer

      launch_via Launchers::Zookeeper::Wait
    end
  end

  namespace :debug do
    action :pp do
      attribute :value
      launch_via Launchers::Debug
    end
  end

  namespace :io do
    resource :get_uri do
      attribute :uri, type: String

      attr_accessor :contents
      launch_via Launchers::IO
    end
  end

  namespace :mustache do
    resource :binding do
      attribute :name, type: String
      attribute :value

      def initialize(name, value)
        self[:name] = name
        self[:value] = value
      end
    end

    resource :render do
      attribute :template, type: String
      collection :bindings, type: 'mustache/binding'


      attribute :contents
      launch_via Launchers::Mustache
    end
  end

  namespace :coreos do
    action :validate_cloud_config do
      attribute :contents

      launch_via Launchers::Validators::CoreOS
    end
  end
end
