require 'warnings_filter'
require 'minitest/pride'
require 'minitest/autorun'

require 'ui'

class TestableUI < UI
  class UITriggeredFailure < StandardError
    def initialize(exitstatus, message=nil)
      @exitstatus, @message = exitstatus, message
    end

    def to_s
      format('UI#exit exitstatus=%i message=%s', @exitstatus, @message.inspect)
    end
  end

  def fail(message=nil, exitstatus=1)
    error(message) if message
    raise UITriggeredFailure.new(exitstatus, message)
  end

  def exit(exitstatus=1)
    raise UITriggeredFailure.new(exitstatus) unless exitstatus == 0
  end
end
