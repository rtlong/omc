# Orchestral Manoeuvres in the Cloud

This tool is a bit like AWS CloudFormation but acts across multiple Cloud providers. "Stacks" are definitions of a series of related cloud resources and actions upon those resources.

## Installation

At this early stage, installation is a little clunky:

1. Clone the repo

2. `bundle install`

3. "Install" the `omc` command to your path. A symlink from some directory on your `$PATH` to the `<cloned omc dir>/bin/omc` should work well.

    For example, if your `$PATH` includes `$HOME/.local/bin`, this command would symlink `omc` into it.

    ```shell
    ln -s $PWD/bin/omc $HOME/.local/bin/omc
    ```

## Usage

4. Create a "stack definition". [See the stack definition docs for help on this.](docs/stack_definitions.md)

5. Launch the stack:

    ```shell
    omc launch stacks/stack_name.scm
    ```

## Tips

You shouldn't need any external dependencies, but the tool does expect you to have set all relevant services' API credentials as a few environment variables. These are loaded lazily, so you shouldn't need credentials set for any service which a stack does not use.

### DNSMadeEasy

Get your API credentials here: <https://cp.dnsmadeeasy.com/account/info>

(Note that API credentials are only available for the primary "owner" account and not for additional users.)

Then set the following environment variables:

```shell
DNSMADEEASY_API_TOKEN
DNSMADEEASY_API_SECRET
```

### RightScale

Get your personal API token refresh token from the RightScale control panel.

Then set the following environment variables:

```shell
RIGHTSCALE_REFRESH_TOKEN
RIGHTSCALE_API_ENDPOINT
RIGHTSCALE_ACCOUNT_ID
```

### AWS

You need to create an access key in IAM, then set the following environment variables respectively:

```shell
AWS_ACCESS_KEY_ID
AWS_SECRET_ACCESS_KEY
```

You can also set these up in `~/.aws/credentials` as would be used by the `aws` CLI tool (created by said tool by running `aws configure`)

### DigitalOcean

See <https://cloud.digitalocean.com/settings/applications>

```
DIGITALOCEAN_API_TOKEN
```
