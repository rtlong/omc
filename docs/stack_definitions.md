# Stack Definitions

In OMC, stacks are defined using a DSL built upon Scheme syntax. It's pretty
standard LISP syntax, although the semantics are likely very peculiar.

Here are two example stack definitions which set up a CoreOS cluster

- [AWS CloudFormation](https://github.com/bridgefog/coreos/blob/master/omc/coreos-stack.aws.scm)
- [DigitalOcean](https://github.com/bridgefog/coreos/blob/master/omc/coreos-stack.digitalocean.scm)
